const fs = require('fs-extra');
const concat = require('concat');

(async function build() {    
const files = [
        './dist/Login/runtime.js',
        './dist/Login/polyfills.js',
        './dist/Login/main.js'
    ]   
await fs.ensureDir('Login')    
await concat(files, 'public/elements.js')})();