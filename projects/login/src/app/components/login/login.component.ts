import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  ElementRef,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class LoginComponent implements OnInit {
  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder
  ) {}

  @Input('title') title = 'Sign In';
  @Output() loggedIn = new EventEmitter();

  loginForm: any;

  ngOnInit(): void {
    this.initForm();
  }

  login(): void {
    const requesPayload = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };
    this.loginService.login(requesPayload).subscribe(
      (responseData) => {
        console.log(responseData);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  initForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.pattern('^[a-zA-Z]+$'),
        ],
      ],
    });
  }

  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }
}
