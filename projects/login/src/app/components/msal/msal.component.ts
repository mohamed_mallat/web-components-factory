import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Output } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { EventEmitter } from '@angular/core';

type ProfileType = {
  givenName: string;
  surname: string;
  userPrincipalName: string;
  id: string;
};

@Component({
  selector: 'app-msal',
  templateUrl: './msal.component.html',
  styleUrls: ['./msal.component.scss'],
})
export class MsalComponent implements OnInit {
  @Input('GRAPH_ENDPOINT') GRAPH_ENDPOINT = '';
  @Output() loggedInSuccess = new EventEmitter();

  isLogedIn: boolean = false;
  loginDisplay: boolean = false;
  isIframe = false;

  profile = {
    givenName: 'Sadok',
    surname: 'Morad Mhiri',
    userPrincipalName: 'Sadok Morad Mhiri',
    id: '0000123456789',
  };

  constructor(private authService: MsalService, private http: HttpClient) {}

  ngOnInit(): void {
    this.isIframe = window !== window.parent && !window.opener;
  }

  login() {
    this.authService.loginPopup().subscribe(
      (result) => {
        debugger;
        this.getProfile();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  setLoginDisplay() {
    this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  }

  getProfile() {
    this.http.get(this.GRAPH_ENDPOINT).subscribe((profile: any) => {
      this.profile = profile;
    });
  }
}
