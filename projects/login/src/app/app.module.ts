import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { AppComponent } from './app.component';
import { createCustomElement } from '@angular/elements';

import { MsalGuard, MsalInterceptor, MsalModule } from '@azure/msal-angular';
import { InteractionType, PublicClientApplication } from '@azure/msal-browser';
import { MsalComponent } from './components/msal/msal.component';

const isIE =
  window.navigator.userAgent.indexOf('MSIE ') > -1 ||
  window.navigator.userAgent.indexOf('Trident/') > -1;

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AppComponent,
    MsalComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    MsalModule.forRoot(
      new PublicClientApplication({
        auth: {
          clientId: 'Enter_the_Application_Id_here',
          authority:
            'Enter_the_Cloud_Instance_Id_Here Enter_the_Tenant_Info_Here',
          redirectUri: 'Enter_the_Redirect_Uri_Here',
        },
        cache: {
          cacheLocation: 'localStorage',
          storeAuthStateInCookie: isIE,
        },
      }),
      {
        interactionType: InteractionType.Redirect,
        authRequest: {
          scopes: ['user.read'],
        },
      },
      {
        interactionType: InteractionType.Redirect,
        protectedResourceMap: new Map([
          ['Enter_the_Graph_Endpoint_Here/v1.0/me', ['user.read']],
        ]),
      }
    ),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true,
    },
    MsalGuard,
  ],
  entryComponents: [LoginComponent, RegisterComponent],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private injector: Injector) {
    const elements: any[] = [
      [LoginComponent, 'jul-login'],
      [RegisterComponent, 'jul-register'],
    ];
    for (const [component, name] of elements) {
      const el = createCustomElement(component, { injector: this.injector });
      customElements.define(name, el);
    }
  }
  ngDoBootstrap() {}
}
